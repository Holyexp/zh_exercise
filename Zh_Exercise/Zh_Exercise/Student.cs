﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Zh_Exercise
{
    class Student
    {       
        public string Name { get; set; }
        public string Course { get; set; }
        public int Mark { get; set; }
        public string Type { get; set; }
        public Student(string name, string course, int mark, string type)
        {
            Name = name;
            Course = course;
            Mark = mark;
            Type = type;

        }
        public static List<Student> XMLToList (string FileName)
        {
            XDocument xmlDoc = XDocument.Load(FileName);
            var allStudents = xmlDoc.Root.Elements("student").ToList();
            List<Student> studentList = new List<Student>();
            foreach (var item in allStudents)
            {
                studentList.Add(new Student(item.Element("name").Value, item.Element("course").Value,
                                  Int32.Parse(item.Element("mark").Value), item.Element("type").Value));
            }
            foreach (var item in studentList) //Not required, just test
            {
                Console.WriteLine("Name:{0}, Course:{1}, Mark:{2}, Type:{3}",item.Name, item.Course, item.Mark, item.Type);
            }
            return studentList;
        }

    }
}
