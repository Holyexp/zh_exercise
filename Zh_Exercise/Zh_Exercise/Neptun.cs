﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zh_Exercise
{
    class Neptun
    {
        List<Student> Students { get; set; }
        StudentEntities StudentEntities { get; set; }
        public Neptun(List<Student> studList, StudentEntities studentEntities)
        {
            Students = studList;
            StudentEntities = studentEntities;
        }

        public void OpreStudents()
        {
            IEnumerable<Student> opreStudents = Students.FindAll(x => x.Course == "opre");
            LinqResultConsole.DisplayResult(opreStudents);
        }
        public void OpreStudentsName()
        {
            IEnumerable<string> opreStudentsName = Students.FindAll(x => x.Course == "opre").Select(x => x.Name);
            LinqResultConsole.DisplayResult(opreStudentsName);

        }
        public void CourseAverages()
        {
            var q = from m in Students
                    group m by new
                    {
                        m.Course
                    } into n
                    select new
                    {
                        Courses = n.Key.Course,
                        Average = n.Average(p => p.Mark)
                    };
            LinqResultConsole.DisplayResult(q); //Not working, yet
        }
        public void JoinedCourseAverages()
        {
            var q = from stud in Students
                    join StudentEntities in StudentEntities.course
                    on stud.Course equals StudentEntities.courseShortName
                    group stud by StudentEntities.courseLongName into studentGrouped
                    select new
                    {
                        LongNameCourse = studentGrouped.Key,
                        Average = studentGrouped.Average( m => m.Mark)
                    };
            LinqResultConsole.DisplayResult(q); //Not working, yet
        }
        public void CoursesAttendedTo()
        {
            var q = from student in Students
                    join StudentEntities in StudentEntities.course
                    on student.Course equals StudentEntities.courseShortName
                    select new
                    {
                        student.Name,
                        StudentEntities.courseLongName
                    };
            LinqResultConsole.DisplayResult(q); //Not working, yet
        }
    }
}
