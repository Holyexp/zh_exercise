﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Zh_Exercise
{
    static class LinqResultConsole
    {
        public static void DisplayResult<T>(IEnumerable<T> result) // TODO: Doesn't work properly
        {
            Type t = typeof(T);
            switch (t.ToString())
            {
                case "System.String":
                case "System.Int32":
                    Console.WriteLine("results: ");
                    foreach (var item in result)
                    {
                        Console.Write(item + "\t");
                    }
                    Console.WriteLine("\n");
                    break;
                default:
                    PropertyInfo[] pis = t.GetProperties();                   
                    foreach (var item in pis)
                    {
                        Console.Write(item.Name + "\t");
                    }
                    Console.WriteLine();
                    foreach (var item in result)
                    {
                        foreach (var prop in pis)
                        {
                            Console.Write(prop.GetValue(item) + "\t");
                        }
                        Console.WriteLine();
                    }       
                    Console.WriteLine();
                    break;
            }

        }
    }
}
