﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zh_Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> studentList = Student.XMLToList("students.xml");
            StudentEntities stEntity = new StudentEntities();
            Neptun ntn = new Neptun(studentList, stEntity);
            Console.WriteLine("\n1. feladat");
            ntn.OpreStudents();
            Console.WriteLine("2. feladat");

            ntn.OpreStudentsName();
            Console.WriteLine("3. feladat");

            ntn.CourseAverages();
            Console.WriteLine("4. feladat");

            ntn.JoinedCourseAverages();
            Console.WriteLine("5. feladat");

            ntn.CoursesAttendedTo();
        }
    }
}
